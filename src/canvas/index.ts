import init from './_init'

const smallCanvasHeight = 60
const w = window.innerWidth

export const main = init('#mainCanvas', w, window.innerHeight - smallCanvasHeight * 2 - 30)
export const small = init('#smallCanvas', w, smallCanvasHeight)
export const activeLines = init('#activeLinesCanvas', w, smallCanvasHeight)

export default {
  main,
  small,
  activeLines,
}
