export default (id, width, height) => {
  const canvas = document.querySelector(id)
  canvas.width = width
  canvas.height = height
  const cx = canvas.getContext("2d")

  if (window.devicePixelRatio > 1) {
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    canvas.width = canvasWidth * window.devicePixelRatio;
    canvas.height = canvasHeight * window.devicePixelRatio;
    canvas.style.width = canvasWidth + 'px';
    canvas.style.height = canvasHeight + 'px';

    cx.scale(window.devicePixelRatio, window.devicePixelRatio);
  }

  return {
    width,
    height,
    canvas,
    cx,
  }
}
