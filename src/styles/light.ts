export default {
  bg: "#fff",
  lightBg: "#ffffff",
  darkBg: "rgba(0, 0, 0, 0.3)",
  fg: "#383838",
  darkFg: "rgba(0, 0, 0, 0.3)",
  sliderInactive: "rgba(9, 12, 17, 0.015)",
  sliderBorders: "rgba(9, 12, 17, 0.08)",
  linesBorder: "rgba(0, 0, 0, 0.3)",
}
