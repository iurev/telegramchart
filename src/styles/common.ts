export default {
  fontFamily: 'sans-serif',
  smallFontSize: '14px',
  mediumFontSize: '18px',
  bigFontSize: '20px',
}
