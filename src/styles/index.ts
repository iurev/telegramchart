import common from './common'
import dark from './dark'
import light from './light'

let result = {
  ...common,
  ...light,
}

export const change = (theme) => {
  if (theme === 'light') {
    (<any>Object).assign(result, light)
  } else {
    (<any>Object).assign(result, dark)
  }
}

export default result
