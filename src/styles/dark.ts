export default {
  bg: "#242f3f",
  lightBg: "#253241",
  darkBg: "#222120",
  fg: "#fff",
  darkFg: "rgba(255, 255, 255, 0.3)",
  sliderInactive: "rgba(9, 12, 17, 0.3)",
  sliderBorders: "rgba(255, 255, 255, 0.15)",
  linesBorder: "rgba(255, 255, 255, 0.3)",
}
