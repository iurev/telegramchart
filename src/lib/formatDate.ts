const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
]

const weekDays = [
  "Mon",
  "Tue",
  "Wed",
  "Thu",
  "Fri",
  "Sat",
  "Sun",
]

export const formatDateShort = (timestamp) => {
  const d = new Date(timestamp)
  const day = d.getDate()
  const month = months[d.getMonth()]
  return `${day} ${month}`
}

export const formatDateLong = (timestamp) => {
  const d = new Date(timestamp)
  const day = d.getDate()
  const month = months[d.getMonth()]
  const weekDay = weekDays[d.getDay()]
  return `${weekDay}, ${month} ${day}`
}
