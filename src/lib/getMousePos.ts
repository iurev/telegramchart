export default (canvas, evt) => {
  var rect = canvas.getBoundingClientRect()
  if (!evt.touches) {
    return {
      x: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width / window.devicePixelRatio,
      y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height / window.devicePixelRatio,
    }
  }
  return {
      x: (evt.touches[0].clientX - rect.left) / (rect.right - rect.left) * canvas.width / window.devicePixelRatio,
      y: (evt.touches[0].clientY - rect.top) / (rect.bottom - rect.top) * canvas.height / window.devicePixelRatio,
  }
}
