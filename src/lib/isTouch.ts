export default () => {
  return 'ontouchstart' in window || navigator.msMaxTouchPoints
}
