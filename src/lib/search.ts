export default (): any => {
  const result = {}
  const splitted = location.search.replace('?', '').split('=')
  splitted.forEach((key, index) => {
    if (index % 2 === 0) {
      result[key] = splitted[index + 1]
    }
  })
  return result
}
