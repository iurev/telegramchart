import chart from './chart'
import slider from './slider'
import activeLines from './activeLines'
import placeholder from './placeholder'
import theme from './theme'

const state = {
  chart,
  slider,
  activeLines,
  placeholder,
  theme,
}

export default state
