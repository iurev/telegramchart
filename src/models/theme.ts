let theme = 'dark'
try {
  const t = localStorage.getItem('theme')
  if (t) theme = t
} catch(_) {}


export default theme
