class Slider {
  private _minX: number = 0.3
  private _maxX: number = 0.6
  private MIN_RANGE = 0.1

  set minX(v: number) {
    if (!this.isMinXValid(v)) return
    if (!this.isRangeValid(v, this._maxX)) return
    this._minX = v
  }

  get minX(): number {
    return this._minX
  }

  set maxX(v: number) {
    if (!this.isMaxXValid(v)) return
    if (!this.isRangeValid(this._minX, v)) return
    this._maxX = v
  }

  get maxX(): number {
    return this._maxX
  }

  setBoth(minX: number, maxX: number) {
    if (this.isMinXValid(minX) && this.isMaxXValid(maxX)) {
      this._minX = minX
      this._maxX = maxX
    }
  }

  isMinXValid(minX: number) {
    return (minX >= 0) && (minX < this._maxX)
  }

  isMaxXValid(maxX: number) {
    return (maxX <= 1) && (maxX > this._minX)
  }

  isRangeValid(minX, maxX) {
    return Slider.range(minX, maxX) >= this.MIN_RANGE
  }

  range() {
    return Slider.range(this._minX, this._maxX)
  }

  static range(minX, maxX) {
    return maxX - minX
  }
}

export default new Slider()
