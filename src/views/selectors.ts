export const getLinesWithColors = (data, activeLines) => {
  const result = {}

  const { colors, columns } = data

  const al = Object.keys(activeLines).filter((key) => activeLines[key].active)

  columns
    .filter(values => values[0] !== 'x')
    .filter(values => (al.indexOf(values[0]) !== -1))
    .map(values => values.slice())
    .forEach(values => {
      const name = values.shift()
      result[name] = {
        color: colors[name],
        values,
      }
    })

  return result
}

export const getSliderLeftRight = ({ canvasWidth, slider}) => {
  const minX = [canvasWidth * slider.minX - 10, 0, 6, 200]
  const maxX = [canvasWidth * slider.maxX - 10, 0, 6, 200]
  return { minX, maxX }
}
