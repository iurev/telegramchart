export default ({
  activeLines,
  width,
  height,
  colors,
}) => {
  const result = []

  const keys = Object.keys(activeLines.values)

  let approxCount = keys.length
  if (approxCount <= 2) {
    approxCount = 3
  }

  let buttonHeight = 40
  if (width <= 359) {
    buttonHeight = 30
  }
  const top = 10
  const padding = 5
  const buttonMargin = 5
  const buttonWidth = (width - (approxCount + 1) * buttonMargin) / approxCount

  keys.forEach((key, index) => {
    let x = index * buttonWidth + buttonMargin * (index + 1)

    const circleRadius = (buttonHeight - padding * 2) / 2
    const circleX = x + circleRadius * 2 - padding
    const circleY = top + circleRadius + padding

    result.push({
      ...activeLines.values[key],
      key,
      textY: circleY,
      textX: circleX + circleRadius + padding,
      buttonY: top,
      buttonX: x,
      buttonWidth: buttonWidth,
      buttonHeight,
      buttonRadius: 40,
      circleRadius,
      circleX,
      circleY,
      circleBg: colors[key],
    })
  })

  return result
}
