import clear from '../lib/clear'
import styles from '../../styles'
import roundedRect from '../lib/roundedRect'
import circle from '../lib/circle'


export default ({
  cnv,
  activeLines,
}) => {
  const {
    canvas,
    cx,
  } = cnv

  clear(cx, canvas)

  activeLines.forEach((line) => {
    cx.strokeStyle = styles.darkFg
    roundedRect(
      cx,
      line.buttonX,
      line.buttonY,
      line.buttonWidth,
      line.buttonHeight,
      line.buttonRadius
    )
    cx.stroke()

    cx.fillStyle = line.circleBg
    cx.strokeStyle = 'transparent'
    circle(
      cx,
      line.circleX,
      line.circleY,
      line.circleRadius
    )
    
    cx.textBaseline = "middle"
    cx.font = `${styles.bigFontSize} ${styles.fontFamily}`
    if (line.active) {
      cx.fillStyle = styles.fg
    } else {
      cx.fillStyle = styles.darkFg
    }
    cx.fillText(line.name, line.textX, line.textY)
  })
}
