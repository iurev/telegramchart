import clear from '../lib/clear'
import styles from '../../styles'
import roundedRect from '../lib/roundedRect'
import circle from '../lib/circle';

const renderPlaceholder = (
  placeholder,
  cx: CanvasRenderingContext2D,
) => {
  if (!placeholder.x) return

  cx.fillStyle = styles.lightBg
  cx.shadowBlur = 15
  cx.shadowColor = styles.darkBg
  roundedRect(
    cx,
    placeholder.panelX,
    placeholder.panelY,
    placeholder.panelWidth,
    placeholder.panelHeight,
    5
  )
  cx.fill()
  cx.shadowBlur = 0

  cx.textBaseline = "top"
  cx.font = `${styles.mediumFontSize} ${styles.fontFamily}`
  cx.fillStyle = styles.fg
  cx.fillText(placeholder.xValue, placeholder.yValues[0].x, placeholder.xX)

  placeholder.yValues.forEach((y) => {
    cx.textBaseline = "top"
    cx.fillStyle = y.color
    cx.font = `600 ${placeholder.countFontSize} ${styles.fontFamily}`
    cx.fillText(y.v, y.x, y.y)
    cx.font = `${styles.mediumFontSize} ${styles.fontFamily}`
    cx.fillText(y.name, y.x + 1, y.y + 25)
  })
}

const renderLines = (data, cx) => {
  Object.keys(data.charts).forEach((key) => {
    const line = data.charts[key]
    cx.strokeStyle = line.color
    cx.lineWidth = data.lineWidth
    cx.beginPath()
    line.values.forEach(point => {
      cx.lineTo(point.x, point.y)
    })
    cx.stroke()
  })
}

const renderXValues = (data, cx) => {
  data.xValues.forEach((v) => {
    cx.textBaseline = "bottom"
    cx.font = `${styles.smallFontSize} ${styles.fontFamily}`
    cx.fillStyle = styles.darkFg
    cx.fillText(v.text, v.x, v.y - 15)
  })
}

const renderYValues = (data, cx) => {
  data.yValues.forEach((v) => {
    cx.textBaseline = "bottom"
    cx.font = `${styles.smallFontSize} ${styles.fontFamily}`
    cx.fillStyle = styles.darkFg
    cx.fillText(v.text, v.xStart, v.y - 10)
  })
}

const renderYLines = (data, cx, canvasWidth) => {
  data.yValues.forEach((v) => {
    cx.strokeStyle = styles.darkFg
    cx.lineWidth = 1
    cx.beginPath()
    cx.moveTo(v.xStart, v.y)
    cx.lineTo(v.xEnd, v.y)
    cx.stroke()
    cx.closePath()
  })
}

const renderPlaceholderLine = (placeholder, cx) => {
  if (!placeholder.x) return

  cx.lineWidth = 1
  cx.strokeStyle = styles.darkFg
  cx.beginPath()
  cx.moveTo(placeholder.x, placeholder.lineYStart)
  cx.lineTo(placeholder.x, placeholder.lineYStop)
  cx.stroke()
  cx.closePath()
}

const renderPlaceholderCircles = (placeholder, lineWidth, cx) => {
  if (!placeholder.x) return

  cx.lineWidth = lineWidth
  placeholder.circles.forEach((c) => {
    cx.strokeStyle = c.color
    cx.fillStyle = styles.bg
    circle(
      cx,
      c.x,
      c.y,
      lineWidth * 2
    )
  })
}

export default ({
  cnv,
  data,
}) => {
  const { canvas, cx, width } = cnv

  clear(cx, canvas)
  renderYLines(data, cx, width)
  renderPlaceholderLine(data.placeholder, cx)
  renderLines(data, cx)
  renderXValues(data, cx)
  renderYValues(data, cx)
  renderPlaceholderCircles(data.placeholder, data.lineWidth, cx)
  renderPlaceholder(data.placeholder, cx)
}
