import convertY from '../../lib/convertY'
import { formatDateShort, formatDateLong } from '../../lib/formatDate'

const mainPadding = 10

const getAllVals = (data, minXIndex, maxXIndex) => {
  let result = []
  Object.keys(data).forEach((name) => {
    const line = data[name]
    const elcopy = line.values.slice(minXIndex, maxXIndex)
    result = result.concat(elcopy)
  });
  return result
}

const nLength = (n) => {
  return n.toString().length
}

const pow = (x, y) => {
  return Math.pow(x, y)
}

const getYMinMax = (data, minXIndex, maxXIndex) => {
  const allValues = getAllVals(data, minXIndex, maxXIndex)
  let minY = Math.min(...allValues)
  let maxY = Math.max(...allValues)
  const maxYLength = nLength(maxY) - 1
  minY = Math.floor(minY / pow(10, maxYLength)) * pow(10, maxYLength)
  maxY = Math.ceil(maxY / pow(10, maxYLength)) * pow(10, maxYLength)
  return {
    minY,
    maxY,
  }
}

const getPlaceholder = (
  placeholder,
  _minXIndex,
  _maxXIndex,
  indexToCanvasCoeff,
  data,
  xValues,
  width,
  height,
  slider,
  minY,
  maxY,
  _heiht,
) => {
  const result = {
    x: null,
    xX: null,
    xValue: null,
    yValues: [],
    panelX: null,
    panelY: null,
    panelWidth: null,
    panelHeight: 105,
    lineYStart: null,
    lineYStop: null,
    countFontSize: null,
    circles: []
  }

  const placeholderStyles = {
    padding: 15,
    top: placeholder.y > (height / 2) ? 20 : (height - 60 - result.panelHeight)
  }
  const top = placeholderStyles.top + placeholderStyles.padding

  if (!(placeholder.index && (placeholder.index >= _minXIndex) && (placeholder.index <= _maxXIndex))) {
    return result
  }

  result.x = (placeholder.index * indexToCanvasCoeff - _minXIndex * indexToCanvasCoeff) / slider.range()
  result.xValue = formatDateLong(xValues[placeholder.index])

  const linesCount = Object.keys(data).length
  let approxCharWidth = 14

  let correctionCoeff = 0
  let panelWidth = 0
  let maxLength = 0

  const keys = Object.keys(data)
  keys.forEach((key) => {
    const line = data[key]
    const v = line.values[placeholder.index]
    const l = v.toString().length
    maxLength = maxLength > l ? maxLength : l
    return maxLength
  })
  
  if (maxLength * keys.length >= 25) {
    approxCharWidth = 10.5
  }

  keys.forEach((name, i) => {
    const line = data[name]
    const v = line.values[placeholder.index]
    const l = v.toString().length
    const approxWidthLegend = l * approxCharWidth

    panelWidth += approxWidthLegend

    const half =  Math.round(approxWidthLegend * linesCount)
    let x = result.x + i * approxWidthLegend - half

    if (i === 0) {
      if ((x - placeholderStyles.padding + 10) < 0) {
        correctionCoeff = -(x - placeholderStyles.padding - 10)
      }
    } else if (i === (linesCount - 1)) {
      if ((x + 100) > width) {
        correctionCoeff = width - (x + approxWidthLegend) + placeholderStyles.padding - 100
      }
    }

    result.circles.push({
      y: convertY((v - minY) * _heiht / maxY, _heiht),
      x: result.x,
      color: line.color,
    })

    result.yValues.push({
      v,
      name,
      x,
      y: top + 35,
      color: line.color
    })
  })

  result.yValues.forEach((y, i) => {
    y.x = y.x + correctionCoeff
    if (i === 0) {
      result.panelX = y.x - placeholderStyles.padding
    }
  })
  panelWidth = Math.max(panelWidth, 10 * result.xValue.length)
  result.panelWidth = panelWidth + placeholderStyles.padding * 2
  result.panelY = placeholderStyles.top
  result.countFontSize = maxLength > 5 ? '14px' : '20px'
  result.xX = top
  result.lineYStart = 0
  result.lineYStop = _heiht

  return result
}

const getXLegends = (
  _minXIndex,
  _maxXIndex,
  xValues,
  slider,
  width,
  height,
) => {
  const result = []
  let nextIndex = _minXIndex

  xValues.forEach((v, index) => {
    if (index < _minXIndex) return
    if (index > _maxXIndex) return
    if (index !== nextIndex) return

    nextIndex = Math.round(nextIndex + ((_maxXIndex - _minXIndex) / 5))

    const indexToCanvasCoeff = width / xValues.length

    result.push({
      text: formatDateShort(v),
      x: (index * indexToCanvasCoeff - _minXIndex * indexToCanvasCoeff) / slider.range(),
      y: height
    })
  })

  return result
}

const getLineWidth = (range) => {
  const result = 1 / range
  if (result >= 3) return 3
  return result
}

const getYValues = (
  minY,
  maxY,
  _heiht,
  width,
) => {
  const result = []

  let yLabel = minY

  const diff = (maxY - minY) / 5

  while (yLabel <= maxY) {
    result.push({
      text: yLabel,
      xStart: mainPadding,
      xEnd: width - mainPadding,
      y: convertY((yLabel - minY) * _heiht / maxY, _heiht),
    })
    yLabel += diff
  }

  return result
}

export default ({
  data,
  width,
  height,
  slider,
  xValues,
  placeholder,
}) => {
  const _minXIndex = Math.round(xValues.length * slider.minX)
  const _maxXIndex = Math.round(xValues.length * slider.maxX)

  const _heiht = height - 40

  const {
    minY,
    maxY
  } = getYMinMax(data, _minXIndex, _maxXIndex)

  const result = {
    xValues: [],
    yValues: [],
    charts: {},
    placeholder: {},
    lineWidth: getLineWidth(slider.range())
  }

  const indexToCanvasCoeff = width / xValues.length

  Object.keys(data).forEach(name => {
    const line = data[name]

    const values = line.values.map((y, index) => {
      return {
        x: (index * indexToCanvasCoeff - _minXIndex * indexToCanvasCoeff) / slider.range(),
        y: convertY((y - minY) * _heiht / maxY, _heiht),
      }
    })

    result.charts[name] = {
      color: line.color,
      values,
    }
  })

  result.xValues = getXLegends(
    _minXIndex,
    _maxXIndex,
    xValues,
    slider,
    width,
    height,
  )
  result.yValues = getYValues(
    minY,
    maxY,
    _heiht,
    width,
  )

  result.placeholder = getPlaceholder(
    placeholder,
    _minXIndex,
    _maxXIndex,
    indexToCanvasCoeff,
    data,
    xValues,
    width,
    height,
    slider,
    minY,
    maxY,
    _heiht,
  )

  return result
}
