import clear from '../lib/clear'
import styles from '../../styles'

const renderControls = (cx, slider) => {
  cx.fillStyle = styles.sliderBorders
  cx.fillRect(...slider.minX)
  cx.fillRect(...slider.maxX)
}

const renderBorder = (
  cx,
  startX,
  startY,
  endX,
  endY,
) => {
  cx.strokeStyle = styles.sliderBorders
  cx.lineWidth = 4
  cx.beginPath()
  cx.moveTo(startX, startY)
  cx.lineTo(endX, endY)
  cx.stroke()
}

const renderLines = (
  cx,
  data,
) => {
  Object.keys(data).forEach((key) => {
    const line = data[key]
    cx.strokeStyle = line.color
    cx.lineWidth = 1
    cx.beginPath()
    line.values.forEach(point => {
      cx.lineTo(point.x, point.y)
    })
    cx.stroke()
  })
}

const renderInactive = (
  cx,
  slider,
  width,
  height,
) => {
  cx.fillStyle = styles.sliderInactive
  cx.fillRect(0, 0, slider.minX[0], height)
  cx.fillRect(slider.maxX[0] + slider.maxX[2], 0, width, height)
}

export default ({
  cnv,
  data,
  slider,
}) => {
  const {
    canvas,
    cx,
    width,
    height,
  } = cnv

  clear(cx, canvas)
  renderControls(cx, slider)

  cx.beginPath()

  renderBorder(
    cx,
    slider.minX[0] + slider.minX[2],
    slider.minX[1],
    slider.maxX[0],
    slider.maxX[1]
  )

  renderBorder(
    cx,
    slider.minX[0] + slider.minX[2],
    height,
    slider.maxX[0],
    height
  )

  renderLines(cx, data)

  renderInactive(cx, slider, width, height)
}
