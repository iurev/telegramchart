import convertY from '../../lib/convertY'

const getAllVals = (data) => {
  let result = []
  data.columns.forEach(element => {
    if (element[0] == 'x') return
    const elcopy = element.slice()
    elcopy.shift()
    result = result.concat(elcopy)
  });
  return result
}

const getYMinMax = (data) => {
  const allValues = getAllVals(data)
  const minY = Math.min(...allValues)
  const maxY = Math.max(...allValues)
  return {
    minY,
    maxY,
  }
}

export default ({
  data,
  width,
  height,
}) => {

  const {
    minY,
    maxY,
  } = getYMinMax(data)

  const result = {}

  for (let col = 1; col < data.columns.length; col++) {
    let d: Array<any> = data.columns[col].slice()
    let name: string = d.shift()
    if (name === 'x') continue
    result[name] = {
      color: data.colors[name],
      values: [],
    }
    const length = d.length

    result[name].values.push({
      x: 0,
      y: convertY(d[1], height),
    })
    for (let x = 2; x < length; x += 1) {
      result[name].values.push({
        x: (x - 2) * width / length,
        y: convertY((d[x] - minY) * height / maxY, height),
      })
    }
  }

  return result
}
