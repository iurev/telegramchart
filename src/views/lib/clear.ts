import styles from '../../styles'

export default (cx, canvas) => {
  cx.clearRect(0, 0, canvas.width, canvas.height)
  cx.fillStyle = styles.bg
  cx.fillRect(0, 0, canvas.width, canvas.height)
}
