export default (cx, centerX, centerY, radius) => {
  cx.beginPath()
  cx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false)
  cx.fill()
  cx.stroke()
}
