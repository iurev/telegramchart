export default (cx, x, y, w, h, r) => {
  if (w < 2 * r) r = w / 2
  if (h < 2 * r) r = h / 2
  cx.beginPath()
  cx.moveTo(x + r, y)
  cx.arcTo(x + w, y, x + w, y + h, r)
  cx.arcTo(x + w, y + h, x, y + h, r)
  cx.arcTo(x, y + h, x, y, r)
  cx.arcTo(x, y, x + w, y, r)
  cx.closePath()
}
