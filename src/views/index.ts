import main from './main/index'
import small from './small/index'
import activeLines from './activeLines/index'

export default {
  main,
  small,
  activeLines,
}
