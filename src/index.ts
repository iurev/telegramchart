import canvas from './canvas'
import controllers from './controllers'
import views from './views'
import models from './models'
import s from './views/small/selector'
import mainSelector from './views/main/selector'
import activeLinesSelector from './views/activeLines/selector'
import { getLinesWithColors, getSliderLeftRight } from './views/selectors'
import { change as themeChange } from './styles'
import search from './lib/search'

let xValues = null


const request = () => {
  const chartId = search().chart
  if (!chartId) location.href = '/'

  fetch('/task/chart_data.json')
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    models.chart = myJson[chartId]

    xValues = [...models.chart.columns[0]]
    xValues.shift()

    init()
    renderMain()
    renderSmall()
    renderActiveLines()
  })
}

if (window.location.pathname.indexOf('show') !== -1) {
  request()
}

const init = () => {
  models.activeLines.values = {}
  
  Object.keys(models.chart.names).forEach((key) => {
    models.activeLines.values[key] = {
      name: models.chart.names[key],
      active: true,
    }
  })
}


const renderMain = () => {
  const data2 = mainSelector({
    data: getLinesWithColors(models.chart, models.activeLines.values),
    width: canvas.main.width,
    height: canvas.main.height,
    slider: models.slider,
    placeholder: models.placeholder,
    xValues,
  })
  
  views.main({ cnv: canvas.main, data: data2 })
}

const renderSmall = () => {
  const data = s({
    data: models.chart,
    width: canvas.small.width,
    height: canvas.small.height,
  })
  views.small({
    cnv: canvas.small,
    data,
    slider: getSliderLeftRight({
      canvasWidth: canvas.small.width,
      slider: models.slider,
    })
  })
}

const renderActiveLines = () => {
  const activeLines = activeLinesSelector({
    activeLines: models.activeLines,
    width: canvas.small.width,
    height: canvas.small.height,
    colors: models.chart.colors,
  })
  views.activeLines({
    cnv: canvas.activeLines,
    activeLines,
  })
}

const isIn = (point, rect) => {
  return (point.x >= rect[0]) && (point.x <= (rect[0] + rect[2]))
}

let active = null
let startPoint = null

const change = ({ x, y}) => {
  if (!active) return
  if (active !== 'center') {
    models.slider[active] = x / canvas.small.width
  } else {
    models.slider.setBoth(
      models.slider.minX + (x - startPoint.x) / canvas.small.width,
      models.slider.maxX + (x - startPoint.x) / canvas.small.width
    )
    startPoint = { x, y }
  }

  renderMain()
  renderSmall()
}

const start = ({ x, y }) => {
  const pins = getSliderLeftRight({
    canvasWidth: canvas.small.width,
    slider: models.slider,
  })
  active = null
  Object.keys(pins).forEach((name) => {
    if (isIn({ x, y }, pins[name])) {
      active = name
    }
  })
  if (!active) {
    if (isIn({ x, y }, [pins.minX[0], null, pins.maxX[0], null])) {
      active = 'center'
      startPoint = { x, y }
    }
  }
}

const end = () => {
  active = null
  startPoint = null
}

controllers.small(change, start, end)

const click = ({ x, y }) => {
  const activeLines = activeLinesSelector({
    activeLines: models.activeLines,
    width: canvas.small.width,
    height: canvas.small.height,
    colors: models.chart.colors
  })
  activeLines.forEach((line) => {
    const result = isIn({ x, y }, [line.buttonX, null, line.buttonWidth, null]) 
    if (result) {
      models.activeLines.values[line.key].active = !models.activeLines.values[line.key].active
      const keys = Object.keys(models.activeLines.values)
      if (!keys.filter(key => models.activeLines.values[key].active).length) {
        keys
          .filter(key => key !== line.key)
          .forEach(key => {
            models.activeLines.values[key].active = true
          })
      }
      renderActiveLines()
      renderMain()
    }
  }) 
}


controllers.activeLines(click)

const mainStart = ({ x, y }) => {
  const _minXIndex = Math.round(xValues.length * models.slider.minX)
  const _maxXIndex = Math.round(xValues.length * models.slider.maxX)
  const diff = _maxXIndex - _minXIndex
  const clickedIndex = _minXIndex + Math.round(x / (canvas.main.width / diff))
  models.placeholder = {
    index: clickedIndex,
    y,
  }

  renderMain()
}

controllers.main(mainStart, mainStart, () => {})

const setTheme = (theme) => {
  const body = document.body
  if (theme === 'light') {
    body.classList.remove('dark')
    models.theme = 'light'
    body.classList.add(models.theme)
  } else {
    body.classList.remove('light')
    models.theme = 'dark'
    body.classList.add(models.theme)
  }
  try {
    localStorage.setItem('theme', theme)
  } catch(_) {}
  themeChange(models.theme)
}

setTheme(models.theme)

controllers.theme(() => {
  if (models.theme === 'dark') {
    setTheme('light')
  } else {
    setTheme('dark')
  }
  renderMain()
  renderSmall()
  renderActiveLines()
})
