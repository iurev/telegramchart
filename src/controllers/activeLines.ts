import { activeLines } from '../canvas'
import getMousePos from '../lib/getMousePos'
import isTouch from '../lib/isTouch'

export default (click: Function) => {
  const event = isTouch() ? 'touchstart' : 'mousedown'
  activeLines.canvas.addEventListener(event, (e) => {
    const { x, y } = getMousePos(activeLines.canvas, e)
    click({ x, y })
  })
}
