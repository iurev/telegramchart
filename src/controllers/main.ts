import { main } from '../canvas'
import getMousePos from '../lib/getMousePos'
import isTouch from '../lib/isTouch'

export default (change: Function, start: Function, end: Function) => {
  const mousemove = (e) => {
    const { x, y } = getMousePos(main.canvas, e)
    change({ x, y })
  }

  const mouseup = () => {
    end()
    main.canvas.removeEventListener('touchmove', mousemove)
    main.canvas.removeEventListener('touchend', mousemove)
    main.canvas.removeEventListener('mousemove', mousemove)
    main.canvas.removeEventListener('mouseup', mousemove)
  }

  if (isTouch()) {
    main.canvas.addEventListener('touchstart', (e) => {
      const { x, y } = getMousePos(main.canvas, e)
      start({ x, y })
  
      main.canvas.addEventListener('touchmove', mousemove)
      main.canvas.addEventListener('touchend', mouseup)
    })
  } else {
    main.canvas.addEventListener('mousedown', (e) => {
      const { x, y } = getMousePos(main.canvas, e)
      start({ x, y })
  
      main.canvas.addEventListener('mousemove', mousemove)
      main.canvas.addEventListener('mouseup', mouseup)
    })
  }
}
