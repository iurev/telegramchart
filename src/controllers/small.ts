import { small } from '../canvas'
import getMousePos from '../lib/getMousePos'
import isTouch from '../lib/isTouch'

export default (change: Function, start: Function, end: Function) => {
  const mousemove = (e) => {
    const { x, y } = getMousePos(small.canvas, e)
    change({ x, y })
  }

  const mouseup = () => {
    end()
    small.canvas.removeEventListener('touchmove', mousemove)
    small.canvas.removeEventListener('touchend', mousemove)
    small.canvas.removeEventListener('mousemove', mousemove)
    small.canvas.removeEventListener('mouseup', mousemove)
  }

  if (!isTouch()) {
    small.canvas.addEventListener('mousedown', (e) => {
      const { x, y } = getMousePos(small.canvas, e)
      start({ x, y })
  
      small.canvas.addEventListener('mousemove', mousemove)
      small.canvas.addEventListener('mouseup', mouseup)
    })
  } else {
    small.canvas.addEventListener('touchstart', (e) => {
      const { x, y } = getMousePos(small.canvas, e)
      start({ x, y })
  
      small.canvas.addEventListener('touchmove', mousemove)
      small.canvas.addEventListener('touchend', mouseup)
    })
  }
}
