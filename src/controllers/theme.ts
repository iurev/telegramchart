export default (click: Function) => {
  document.querySelector('.jsToggleTheme').addEventListener('click', () => {
    click()
  })
}
