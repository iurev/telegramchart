import small from './small'
import activeLines from './activeLines'
import main from './main'
import theme from './theme'

export default {
  small,
  activeLines,
  main,
  theme,
}
