module.exports = {
    mode: process.env.NODE_ENV || "development",
    devtool: process.env.NODE_ENV === "production" ? false : "inline-source-map",
    entry: "./index.ts",
    output: {
      filename: "index.js"
    },
    resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: [".ts"]
    },
    module: {
      rules: [
        // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
        { test: /\.tsx?$/, loader: "ts-loader" }
      ]
    }
  };
